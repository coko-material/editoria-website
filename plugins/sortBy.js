module.exports = function(eleventyConfig, options) {
  eleventyConfig.addFilter("sortByOrder", function(collection) {
    console.log(collection)
    return collection.sort((a, b) => {
      a.data.order - b.data.order;
    });
  });
};

---
title: "Built by Coko and the Coko Community"
image: "ketida-coko.svg"
part : 9
---

Ketty is built by the Coko development team. Increasingly Coko is also facilitating organisations to extend Ketty to meet their needs as well as extending Ketty on a for-hire basis. The Coko end-user community of Publishers, Copy Editors, Production Editors, Book Designers, Indexers, Authors, Artists, and others such as print on-demand and publishing services organisations, have all been the drivers behind the design of Ketty since the beginning.

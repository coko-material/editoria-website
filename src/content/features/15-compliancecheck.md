---
title: "Standards Compliant"
image: "ketida-compliance-check.svg"
part : 15
---

Ketty adheres to industry standards to store, manage and export your content, and maintain compatibility with other systems.

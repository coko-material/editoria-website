---
title: "Customisable Workflows"
image: "ketida-customisable-wrokflow.svg"
part : 11
---

Ketty offers a range of customisation options for workflows, including the ability to configure user permissions, enable any of the set of features, and even design entirely unique user interfaces for specific needs. This allows for highly tailored and efficient publishing workflows that fit the needs of individual teams and organisations.

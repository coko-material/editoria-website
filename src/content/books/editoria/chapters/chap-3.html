---
title: "Key concepts defined"
class: "component-body  chapter  "
order: 3
---

<p class="">
  The following Editoria key concepts will be referred to throughout this user
  guide.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_ba972af5-13c1-4392-92e7-89b31b56f707"
>
  <strong>Open source software</strong>
</h2>
<p class="">
  Editoria is open source software that anyone can access and adapt for free.
  Open source software is released under a license that grants liberal rights to
  anyone to use, modify, and redistribute a piece of the software's source code.
  Editoria uses the
  <a
    href="https://gitlab.coko.foundation/ketida/ketida/-/blob/master/LICENSE.md"
    rel=""
    target="blank"
    >MIT license</a
  >.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_f80fdcbc-b63d-46cf-a2d0-0fb071bf61ee"
>
  <strong>Single source publishing</strong>
</h2>
<p class="">
  In single source publishing, all members of the publishing team, including
  authors, copy editors, illustrators, and designers, use an online authoring
  system to share the same document source file. At any given time the content
  will be the same, regardless of which team member is working on it.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_27c6cfe4-3a88-4a6b-887d-2372cab52013"
>
  <strong>Single source, multiple outputs</strong>
</h2>
<p class="">
  Editoria is single source multi-output publishing. Content is created in HTML
  (Hypertext Markup Language). HTML is the most common content format in the
  world. It has the advantage that a great number of conversion tools have
  already been built to convert it to other formats. Editoria leverages this so
  you can easily export to EPUB and PDF.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_313bf0ae-b167-4417-b928-c716fa7e48bb"
>
  <strong>Automated typesetting</strong>
</h2>
<p class="">
  In Editoria, automated typesetting refers to a process where content is
  exported using a design template to ensure the results come out with minimal
  alterations required.
</p>
<p class="">
  Editoria uses Paged.js, an automatic typesetting tool developed by the
  <a href="https://pagedmedia.org/" rel="" target="blank"
    >Paged Media Initiative</a
  >.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_63d6d3bb-56f5-4ba4-a189-c943791a123a"
>
  <strong>Semantic markup</strong>
</h2>
<p class="">
  Semantic markup is a method for using named HTML styles to structure content
  in order to reinforce its meaning (semantics). Non-semantic or arbitrary
  styles are typically applied to change how content appears visually. Editoria
  enforces the use of semantic markup preventing users from applying arbitrary
  styles. Arbitrary styles in MS Word files uploaded to Editoria are converted
  to semantic markup. Note that the markup of uploaded files will need to be
  reviewed.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_6624e76d-d029-44a4-9f14-8b24175ee319"
>
  <strong>Workflow</strong>
</h2>
<p class="">
  Workflow refers to the passage of a text through various processes from
  creation to completion. Throughout these processes, various people with
  specific roles (e.g. Author, Production Editor, Copy Editor) perform tasks at
  specific moments. Some workflows are more linear and prescriptive than others.
  Regardless of the size of your team, Editoria offers many new opportunities
  for flexible, intentional design.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_ab348c15-6194-466c-a97e-79960c369d66"
>
  <strong>Track changes</strong>
</h2>
<p class="">
  Track changes here refers to a text-editing tool similar to those found in
  other word processors, such as Microsoft Word. This tool marks deleted or
  added text with underlining and different colors so that users can easily see
  it. Depending on their assigned roles within the team, users can accept or
  reject these edits and make new, tracked revisions to the text.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_3416b463-84c1-4907-bb0e-2bba9a0aaca5"
>
  <strong>Modular</strong>
</h2>
<p class="">
  Editoria is not a single, monolithic system. Its file-ingestion, editing, and
  export components are individual, modular elements within a suite of products
  that publishers can select à la carte to use or not. This also means that each
  individual element can be customized or replaced to meet each publisher's
  needs <em>without</em> compromising the rest of the system, and the community
  can share and collaborate on these component versions.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_0392acc3-b136-49eb-8b58-9b5da381ef1b"
>
  <strong>WYSIWYG</strong>
</h2>
<p class="">
  WYSIWYG, the acronym of “what you see is what you get.” In Editoria, the
  editor component (called Wax) represents the semantic structure of the
  document being edited (showing, for example, which text is a heading and which
  is a block quote). Wax does not show the final look of the text as it will
  appear on screen or in the printed book.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_d07f2dd6-0d2e-466e-858b-09b5399dc8d8"
>
  Team member roles in Editoria
</h2>
<p class="">
  In Editoria, team members may have one of a number of roles including
  Administrator, Global Production Editor, Production Editor, Copy Editor, and
  Author. All users, regardless of role, can add comments to any part of the
  book to which they have been assigned.
</p>
<h3>Administrator</h3>
<p class="">
  The Administrator has access to all of Editoria including all users’ books and
  chapters. The Administrator does not usually have an active role on a book
  project team.
</p>
<h3>Global Production Editors</h3>
<p class="">
  Global Production Editors have access to all of Editoria including all users’
  books and chapters. Global Production Editors can create books and assign team
  members to books, but cannot assign other Global Production Editors. They can
  edit books at any workflow stage.
</p>
<h3>Production Editor</h3>
<p class="">
  Production Editors are project managers and have the highest level of
  permissions for those roles that actively work on book projects. Production
  Editors can manage the teams that are working on their books. Using the
  workflow tool for each Part or Chapter in the Editoria Book Builder, they can
  control what actions team members can take and when they can take them.
  Production Editors can edit, rename, and archive books in the Book Dash. In
  the Editoria Book Builder, they can add Components, Parts, and Chapters;
  upload Word files and media; edit book settings; and export book files.
</p>
<h3>Copy Editor</h3>
<p class="">
  Copy Editors are assigned by the Production Editor for each book. Copy Editors
  see only the books they have been assigned to. They have access to all parts
  of a book during the “Edit” and “Clean Up” stages (determined by the
  Production Editor using the workflow tool in the Book Builder). Copy Editors
  can add new Chapters to a book and turn track changes on and off.
</p>
<h3>Author</h3>
<p class="">
  The Author role has limited permissions for the books to which they have been
  assigned. Authors have access to the parts of a book when the Production
  Editor sets the workflow status to “Reviewing.” Changes made by Authors are
  always tracked.
</p>
<h2
  id="comp-number-4538de7a-525f-4304-835e-1911c2952326_332544ec-31a6-4db1-9a89-3dba3b116cc5"
>
  Anatomy of a book in Editoria
</h2>
<p class="">
  In Editoria, the content of a book is organized into Frontmatter, Body, and
  Backmatter. Frontmatter and Backmatter are made up of different Components.
  Frontmatter might include a table of contents page and copyright page while
  Backmatter might include endnotes or an index. The Body is made up of Parts,
  and Parts are made up of Chapters. Components, Parts, and Chapters can be
  edited. The workflow status of each Part and Chapter can be controlled by the
  Production Editor using the workflow status tool in the Book Builder.
</p>

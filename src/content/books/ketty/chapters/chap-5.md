---
title: "Share and collaborate on a book"
class: "component-body  chapter  "
order: 5
---

Share your book
---------------

In the top-right of the Producer page, you will find the ‘Share’ button, this will open the modal to add collaborators.

![](/images/cdbc3f9b6604_medium.png)

Only the **Book Owner** can manage book collaborators, other book collaborators can only view book permissions.

To add someone as a collaborator on your book:

*   Enter the person’s email address.
    
*   if the person has already signed up, you’ll see their name to select; alternatively, select the invite option.
    
*   Choose the level of access that the person should have: _can edit_ or _can view_, then select ‘Share’. An email invitation will be sent to the person.
    

![](/images/1c984822987d_medium.png)

Share your book with an existing user or invite to join by email

![](/images/e490be6e3ee9_medium.png)

Choose the relevant access level

The book owner can change the type of access or remove a book collaborator using the dropdown next to the person’s name.

![](/images/419dc9c4d92a_medium.png)

Change a collaborator’s access level or remove their access to the book

Collaborate on books
--------------------

Ketty currently supports **asynchronous editing** on the individual chapter level. This means that only one person can edit a specific chapter at a time. This workflow is well-suited to teams who want to control their content creation and production in a step-by-step manner. When someone is working on a chapter their initials will display next to the chapter name, indicating that they have locked that chapter. Others will be able to read the chapter, but won’t be able to make changes at the same time to avoid conflicting edits.

### Collaborators with edit or view access

Collaborators with _edit access_ can edit the book’s content and metadata, view export previews, and download PDF or Epub files, but they cannot save export profiles or connect to print-on-demand services. Collaborators with _view access_ cannot edit the book’s content and metadata, download PDF or Epub files, save export profiles, or connect to print-on-demand services, but they can view export previews.

![](/images/06d9a56a741e_medium.png)

The initials next to the first chapter title indicate the user who is currently editing the chapter.

Keep an eye on the [Ketty Roadmap](https://miro.com/app/board/uXjVP89vFkc=/?share_link_id=271170804884) as we’ll soon be introducing **synchronous (concurrent) editing** which means multiple collabrators with edit-access can edit the same chapter at the same time. Both asynchronous and synchronous editing will be supported in Ketty so that teams can choose the workflow that suits their book project.
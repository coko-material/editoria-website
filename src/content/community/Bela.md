---
title: "Bela Toci (Albania)"
image: "bela.jpg"
part : 8
role: Ketty QA Engineer
---

Bela brings her QA expertise and eye for detail to the Ketty team. She is a Mechatronics Engineer graduate, an open-source enthusiast, environmentalist, an avid reader, and very passionate about open knowledge. 

---
title: "Barbara Rühling (Germany)"
image: "barbara.jpg"
part : 7
role: Content Development Advisor
---


Barbara is the CEO of [Book Sprints](https://www.booksprints.net/), an organisation that runs rapid book development workshops for clients globally. Barbara has facilitated around 50 Book Sprints since 2013 to create handbooks for NGOs, universities, governments, and companies.

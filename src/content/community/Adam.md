---
title: "Adam Hyde (New Zealand)"
image: "adam-hyde.jpg"
part : 1
role: Ketty Community Facilitator  
---

As Coko's founder, Adam brings technical leadership, pioneering insights, and collaborative knowledge production methods to the Ketty community. Adam was awarded the 2015 and 2016 Shuttleworth Fellowship with the goal of building an open source publishing framework.

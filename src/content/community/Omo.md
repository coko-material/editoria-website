---
title: "Omo Oaiya (Nigeria)"
image: "omo.png"
part : 5
role:  Open Academic Publishing Advisor
---

Omo is the Chief Strategy Officer at the West and Central Research and Education Network ([WACREN](https://wacren.net/en/)). He leads the [LIBSENSE](https://libsense.ren.africa/en/) initiative, which brings together African research networks, libraries, and academic communities to promote Open Science and Open Access publishing in Africa. Aspiring to advance digital and open-access book publishing across the continent, Omo is dedicated to leveraging technology to enhance African academia.
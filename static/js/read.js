// nightmode

// let address =
//   window.location.origin +
//   window.location.pathname +
//   window.location.search +
//   window.location.hash;




document.addEventListener(
  "click",
  function (event) {
    // If user either clicks X button OR clicks outside the modal window, then close modal by calling closeModal()
    console.log(event.target.classList);

    if (
      document.querySelector("#showtoc") &&
      document.querySelector("#showtoc").classList.contains("show")
    ) {
      if (!event.target.closest(".book-toc")) {
        toggleTOC();
      }
    }
  },
  false
);

function toggleTOC() {
  document.querySelector(".book-toc").classList.toggle("show");
}

if (document.querySelector("#showtoc")) {
  document.querySelector("#showtoc").addEventListener("click", function () {
    toggleTOC();
  });
}

function nightModeToggle() {
  if (document.documentElement.classList.contains("nightmode")) {
    //   remove night mode

    document.documentElement.classList.remove("nightmode");

    localStorage.setItem("nightmode", false);
  } else if (!document.documentElement.classList.contains("nightmode")) {
    //   add night mode

    document.documentElement.classList.add("nightmode");
    localStorage.setItem("nightmode", true);
  }
}

if (document.querySelector("#nightmode")) {
  document
    .querySelector("#nightmode")
    .addEventListener("click", nightModeToggle);
}

if (document.querySelector("#zoomplus")) {
  document.querySelector("#zoomplus").addEventListener("click", function () {
    const content = document.querySelector("main");
    let contentStyles = getComputedStyle(content);
    let colorValue = contentStyles.getPropertyValue("--text-zoom");
    console.log(colorValue);
    colorValue = parseFloat(colorValue) + 0.1;
    console.log(colorValue);
    content.style.setProperty(`--text-zoom`, `${colorValue}em`);
  });
}

if (document.querySelector("#zoomminus")) {
  document.querySelector("#zoomminus").addEventListener("click", function () {
    const content = document.querySelector("main");
    let contentStyles = getComputedStyle(content);
    let colorValue = contentStyles.getPropertyValue("--text-zoom");
    console.log(colorValue);
    colorValue = parseFloat(colorValue) - 0.1;
    console.log(colorValue);
    content.style.setProperty(`--text-zoom`, `${colorValue}em`);
  });
}
